package javafx.tutorial;

import javafx.application.Application;
import javafx.stage.Stage;

public class MainApp extends Application {

	private Stage primaryStage;
	
	/**
	 * It's started from method launch() from main
	 */
	@Override
	public void start(Stage primaryStage) {
		
	}

	public static void main(String[] args) {
		launch(args);
	}
}
